import {useEffect, useState} from "react";


const fetchApi = async (pathname, body = {}) => {
    const resp = await fetch(pathname, {
        method: "GET",
        redirect: "follow",
    });

    const result = await resp.text();

    if (!result) {
        return;
    }
    return JSON.parse(result);
}

export const useApiGet = (pathname, init, deps = []) => {

    const [[status, result, error], set] = useState(["init", init]);

    const refresh = async () => {
        set(["loading", result]); //keep last data while loading
        try {
            set(["done", await fetchApi(pathname)]);
        } //set new data
        catch (err) {
            console.warn(result, err);
            alert(err.message);
            set(["error", result, err]);
        } //keep last data after error
    }

    useEffect(() => {
        refresh();
    }, deps);


    return [result, status, refresh, error];
}