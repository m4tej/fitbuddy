import {useState} from "react";


const fetchApi = async (pathname, body = {}) => {
    const urlencoded = new URLSearchParams();

    for (let i in body) {
        urlencoded.append(i, body[i]);
    }

    const headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");

    const resp = await fetch(pathname, {
        method: "POST",
        redirect: "follow",
        body: urlencoded,
        headers
    });

    const resultRaw = await resp.text();
    const result = resultRaw ? JSON.parse(resultRaw) : undefined;

    if (resp.status != 200 && resp.status != 204) {
        throw new Error(result?.error || resp.statusText || "unknown error");
    }

    return result;

}

export const useApiPost = (pathname) => {

    const [[status, result, error], set] = useState(["init"]);

    const post = async (body = {}) => {
        set(["loading", result]); //keep last data while loading
        try {
            set(["done", await fetchApi(pathname, body)]);
            return true;
        } //set new data
        catch (err) {
            console.warn(result, err);
            alert(err.message);
            set(["error", result, err]);
            return false;
        } //keep last data after error
    }

    return [post, status, result, error];
}