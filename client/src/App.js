import React, {useState, Context} from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {LoginForm} from "./routes/LoginForm";
import {Home} from "./routes/Home";
import {RegisterForm} from "./routes/RegisterForm";
import {UserProvider} from "./contexts/userContext";
import {LessonForm} from "./routes/LessonForm";
import {LessonsProvider} from "./contexts/lessonsContext";

import "./App.css";

export const App = () => {
    return (
        <React.StrictMode>
            <UserProvider>
                <LessonsProvider>
                    <BrowserRouter>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="/register" element={<RegisterForm/>}/>
                            <Route path="/login" element={<LoginForm/>}/>
                            <Route path="/lesson/:lessonId?" element={<LessonForm/>}/>
                        </Routes>
                    </BrowserRouter>
                </LessonsProvider>
            </UserProvider>
        </React.StrictMode>
    );
}