import React from "react";
import Table from "react-bootstrap/Table";
import {useUser} from "../contexts/userContext";
import {useApiPost} from "../hooks/useApiPost";
import {useLessons} from "../contexts/lessonsContext";
import {Link} from "react-router-dom";


const UserAction = (props) => {
    const {id, name, capacity, date, owner_id, users_id} = props.lesson;
    const user = props.user;

    const {refresh} = useLessons();

    const isJoined = users_id?.includes(user?.id);

    const [post, status, result, error] = useApiPost(isJoined ? "/lesson/leave" : "/lesson/join");

    const toggle = async (ev) => {
        ev.preventDefault();
        await post({lesson_id: id});
        await refresh();
    }

    const label = isJoined ? "Odhlásit se" : "Přihlásit se";
    const className = isJoined ? "leaveLesson" : "joinLesson";

    return <a className={className} href="#" onClick={toggle}>{label}</a>;
}

const OwnerAction = (props) => {
    const {id, name, capacity, date, owner_id, users_id} = props.lesson;
    const {refresh} = useLessons();
    const [post, status, result, error] = useApiPost("/lesson/delete");

    const del = async (ev) => {
        ev.preventDefault();
        if (await post({lesson_id: id})) {
            await refresh();
        }
    }
    
    return (
        <>
            <div className={"updateLesson"}><Link to={"/lesson/" + id}>Upravit</Link></div>
            <div className={"deleteLesson"}><a href={"#"} onClick={del}>Odstranit</a></div>
        </>
    );

};

export const LessonAction = (props) => {
    const {owner_id} = props;
    const {user} = useUser();

    let content;

    if (!user) {
        content = <p>Nejprve se přihlas</p>;
    } else if (owner_id === user.id) {
        content = <OwnerAction user={user} lesson={props}/>;
    } else {
        content = <UserAction user={user} lesson={props}/>;
    }

    return (
        <div className={"LessonAction"}>
            {content}
        </div>
    )
}