import {useUser} from "../contexts/userContext";
import {useApiPost} from "../hooks/useApiPost";


export const Logout = () => {
    const {user, status, refresh, error} = useUser();
    const [post] = useApiPost("/user/logout");

    const logout = async (ev) => {
        ev.preventDefault();
        await post();
        await refresh();
    }

    return (
        <a href={"#"} onClick={logout}>Odhlásit se</a>
    )
}