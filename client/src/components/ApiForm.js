import React from "react";
import {useApiPost} from "../hooks/useApiPost";

export const ApiForm = (props) => {
    const {pathname, onSubmit, buttonLabel, children} = props;
    const [post, status, result, error] = useApiPost(pathname);

    return (
        <form onSubmit={async ev => {
            ev.preventDefault(); // musí být jinak se formulář odešle klasicky jako je tomu v html a php

            const formData = new FormData(ev.target);
            const data = Object.fromEntries(formData.entries());

            if (await post(data) && onSubmit) {
                await onSubmit();
            }
        }} data-status={status}>
            {children}
            <button type={"submit"}>{buttonLabel || "Odeslat"}</button>
        </form>
    );
}