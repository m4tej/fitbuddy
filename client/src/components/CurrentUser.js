import {useUser} from "../contexts/userContext";
import {Logout} from "./Logout";
import {Link} from "react-router-dom";


export const CurrentUser = () => {
    const {user, status, refresh, error} = useUser();

    if (user) {
        return (
            <div className={"CurrentUser"}>
                <div className={"userNickname"}>{user.nickname}</div>
                <div className={"logout"}><Logout/></div>
            </div>
        )
    } else {
        return (
            <div className={"CurrentUser"}>
                <div className={"login"}><Link to={"/login"}>Přihlásit se</Link></div>
                <div className={"register"}><Link to={"/register"}>Registrovat se</Link></div>
            </div>
        )
    }


}