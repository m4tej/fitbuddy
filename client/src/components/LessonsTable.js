import React from "react";
import Table from "react-bootstrap/Table";
import {LessonAction} from "./LessonAction";
import {useApiGet} from "../hooks/useApiGet";
import {useLessons} from "../contexts/lessonsContext";

const Lesson = (props) => {
    const {name, capacity, date, users_id, description} = props;

    return (
        <tr>
            <td>{name}</td>
            <td>{capacity - users_id.length}/{capacity}</td>
            <td>{date}</td>
            <td>{description}</td>
            <td>
                <LessonAction {...props}/>
            </td>
        </tr>
    )
}


export const LessonsTable = () => {
    const {lessons, status, refresh, error} = useLessons();

    if (status === "error") {
        return <p>Error loading lessons: {error.message}</p>;
    }

    if (status !== "done") {
        return <p>Loading lessons...</p>;
    }

    
    return (
        <Table className={"LessonsTable"}>
            <thead>
            <tr>
                <th>Jméno</th>
                <th>Volná místa</th>
                <th>Datum</th>
                <th>Popis</th>
                <th>Akce</th>
            </tr>
            </thead>
            <tbody>
            {lessons.map((lesson) => <Lesson key={lesson.id} {...lesson}/>)}
            </tbody>
        </Table>
    );
}