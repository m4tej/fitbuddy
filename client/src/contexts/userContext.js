import {createContext, useContext} from "react";
import {useApiGet} from "../hooks/useApiGet";

const UserContext = createContext();


export const UserProvider = (props) => {
    const [user, status, refresh, error] = useApiGet("/user/current");

    return (
        <UserContext.Provider value={{user, status, error, refresh}}>
            {status === "done" ? props.children : null}
        </UserContext.Provider>
    )
}

export const useUser = () => {
    return useContext(UserContext);
}