import {createContext, useContext} from "react";
import {useApiGet} from "../hooks/useApiGet";

const LessonsContext = createContext();

export const LessonsProvider = (props) => {
    const [lessons, status, refresh, error] = useApiGet("/lesson/get-all", []);

    return (
        <LessonsContext.Provider value={{lessons, status, error, refresh}}>
            {status === "done" ? props.children : null}
        </LessonsContext.Provider>
    )
}
export const useLessons = () => {
    return useContext(LessonsContext);
}

export const useLesson = (lessonId) => {
    const {lessons, status, error, refresh} = useLessons();
    const lesson = lessons.find(b => b.id == lessonId);
    return {lesson, status, error, refresh}
}