import React from "react";
import {ApiForm} from "../components/ApiForm";
import {useLesson} from "../contexts/lessonsContext";
import {Link, useNavigate, useParams} from "react-router-dom";
import {CurrentUser} from "../components/CurrentUser";

export const LessonForm = () => {
    const {lessonId} = useParams();
    const {lesson, refresh} = useLesson(lessonId);
    const navigate = useNavigate();

    const pathname = !lesson ? "/lesson/create" : "/lesson/update/";
    const onSubmit = async () => {
        await refresh();
        navigate("/");
    }

    return (
        <div className={"LessonForm"}>
            <div className={"nav"}>
                <h1>{lesson ? "Upravit lekci" : "Nová lekce"}</h1>
                <div><Link to={"/"}>Zpět</Link></div>
            </div>
            <ApiForm pathname={pathname} onSubmit={onSubmit} buttonLabel={"Uložit"}>
                <input name={"id"} value={lesson?.id} readOnly style={{display: "none"}}></input>
                <label htmlFor={"name"}>Název</label>
                <input name={"name"} defaultValue={lesson?.name}></input>
                <label htmlFor={"capacity"}>Kapacita</label>
                <input name={"capacity"} type={"number"} defaultValue={lesson?.capacity}></input>
                <label htmlFor={"date"}>Datum</label>
                <input name={"date"} type={"date"} defaultValue={lesson?.date}></input>
                <label htmlFor={"description"}>Popis</label>
                <textarea name={"description"} defaultValue={lesson?.description}></textarea>
            </ApiForm>
        </div>
    );
}