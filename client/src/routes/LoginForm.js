import React from "react";
import {useUser} from "../contexts/userContext";
import {Link, Navigate} from "react-router-dom";
import {ApiForm} from "../components/ApiForm";


export const LoginForm = () => {
    const {user, refresh} = useUser();

    if (user) {
        return <Navigate to={"/"}/>;
    }

    return (
        <div className={"LoginForm"}>
            <div className={"nav"}>
                <h1>{"Přihlásit se"}</h1>
                <div><Link to={"/"}>Zpět</Link></div>
            </div>
            <ApiForm pathname={"user/login"} onSubmit={refresh} buttonLabel={"Přihlásit se"}>
                <label htmlFor={"email"}>Email</label>
                <input name={"email"}></input>
                <label htmlFor={"password"}>Heslo</label>
                <input name={"password"} type={"password"}></input>
            </ApiForm>
        </div>
    )
}