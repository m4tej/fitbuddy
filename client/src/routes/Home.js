import React from "react";

import {CurrentUser} from "../components/CurrentUser";
import {LessonsTable} from "../components/LessonsTable";
import {Link} from "react-router-dom";
import {useUser} from "../contexts/userContext";


export const Home = () => {
    const {user} = useUser();

    return (
        <div className={"Home"}>
            <div className={"nav"}>
                <h1>FitBuddy</h1>
                <CurrentUser/>
            </div>
            <div className={"addLesson"}>{user ? <Link to={"/lesson"}>Přidat lekci</Link> : null}</div>
            <LessonsTable/>
        </div>
    )
}