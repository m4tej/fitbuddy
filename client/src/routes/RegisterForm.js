import React, {useState} from "react";
import {Link, Navigate} from "react-router-dom";
import {useUser} from "../contexts/userContext";
import {ApiForm} from "../components/ApiForm";


export const RegisterForm = () => {
    const {user, refresh} = useUser();

    if (user) {
        return <Navigate to={"/"}/>;
    }

    return (
        <div className={"RegisterForm"}>
            <div className={"nav"}>
                <h1>{"Registrovat se"}</h1>
                <div><Link to={"/"}>Zpět</Link></div>
            </div>
            <ApiForm pathname="/user/register" onSubmit={refresh} buttonLabel={"Registrovat se"}>
                <label htmlFor={"email"}>Email</label>
                <input name={"email"}></input>
                <label htmlFor={"nickname"}>Přezdívka</label>
                <input name={"nickname"}></input>
                <label htmlFor={"password"}>Heslo</label>
                <input name={"password"} type={"password"}></input>
            </ApiForm>
        </div>
    )
}
