"use strict";

const express = require("express");
const session = require('express-session');

const lessonRouter = require("./controller/lesson-controller");
const userRouter = require("./controller/user-controller");

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Použití middleware pro relace
app.use(session({
    secret: 'ft3h4sh5sh4rt5h6sr', // Toto by mělo být něco složitého a tajného
    resave: false,
    saveUninitialized: true
}));

app.use("/lesson", lessonRouter);
app.use("/user", userRouter);


app.listen(8000, () => {
    console.log("Express server listening on port 8000.")
});