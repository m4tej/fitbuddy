"use strict";
const fs = require("fs");
const path = require("path");

const rf = fs.promises.readFile;
const wf = fs.promises.writeFile;

// 1
const DEFAULT_STORAGE_PATH = path.join(__dirname, "storage", "lessons.json");

class LessonsDao {
    constructor(storagePath) {
        this.lessonStoragePath = storagePath ? storagePath : DEFAULT_STORAGE_PATH;
    }

    // 2
    async getLesson(id) {
        let lessons = await this._loadAllLessons();

        const result = lessons.find(b => {
            return b.id == id;
        });

        return result;
    }

    // 3
    async addLesson(lesson) {
        const lessons = await this._loadAllLessons();

        const lastId = lessons[lessons.length - 1].id;
        lesson.id = parseInt(lastId) + 1;

        lessons.push(lesson);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(lessons, null, 2));
            return {status: "OK", data: lesson};
        } catch (e) {
            return {status: "ERROR", error: e};
        }
    }

    async deleteLesson(lesson_id) {
        let lessons = await this._loadAllLessons();

        const lesson = lessons.find(b => {
            return b.id == lesson_id;
        });

        if (!lesson) {
            const e = new Error(`Lesson not found`);
            e.code = "NOT_FOUND";
            throw e;
        }

        lessons = lessons.filter(lesson => lesson.id != lesson_id);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(lessons, null, 2));
            return {status: "OK", data: lesson};
        } catch (e) {
            return {status: "ERROR", error: e};
        }
    }

    async updateLesson(lessonUpdate) {
        const lessons = await this._loadAllLessons();

        const lesson = lessons.find(b => {
            return b.id == lessonUpdate.id;
        });

        if (!lesson) {
            const e = new Error(`Lesson not found`);
            e.code = "NOT_FOUND";
            throw e;
        }

        if (lessonUpdate.owner_id != lesson.owner_id) {
            const e = new Error('User is not owner of this lesson');
            e.code = "UNAUTHORIZED";
            throw e;
        }

        Object.assign(lesson, lessonUpdate);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(lessons, null, 2));
            return {status: "OK", data: lesson};
        } catch (e) {
            return {status: "ERROR", error: e};
        }
    }

    async joinLesson(lesson_id, user_id) {
        const lessons = await this._loadAllLessons();

        const lesson = lessons.find(b => {
            return b.id == lesson_id;
        });

        if (!lesson) {
            const e = new Error(`Lesson not found`);
            e.code = "NOT_FOUND";
            throw e;
        }

        const users_id = lesson.users_id;

        if (lesson.owner_id == user_id) {
            const e = new Error(`User is owner of the lesson.`);
            e.code = "USER_IS_OWNER";
            throw e;
        }

        if (users_id.includes(user_id)) {
            const e = new Error(`This user is already joined to this lesson.`);
            e.code = "ALREADY_JOINED";
            throw e;
        }

        users_id.push(user_id);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(lessons, null, 2));
            return {status: "OK", data: lesson};
        } catch (e) {
            return {status: "ERROR", error: e};
        }
    }

    async leaveLesson(lesson_id, user_id) {
        const lessons = await this._loadAllLessons();

        const lesson = lessons.find(b => {
            return b.id == lesson_id;
        });

        if (!lesson) {
            const e = new Error(`Lesson not found`);
            e.code = "NOT_FOUND";
            throw e;
        }

        const users_id = lesson.users_id;

        if (!users_id.includes(user_id)) {
            const e = new Error(`This user is not joined to this lesson.`);
            e.code = "NOT_JOINED";
            throw e;
        }

        lesson.users_id = users_id.filter(id => id != user_id);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(lessons, null, 2));
            return {status: "OK", data: lesson};
        } catch (e) {
            return {status: "ERROR", error: e};
        }
    }

    // 4
    async _loadAllLessons() {
        let lessons;
        try {
            lessons = JSON.parse(await rf(this._getStorageLocation()));
        } catch (e) {
            if (e.code === 'ENOENT') {
                console.info("No storage found, initializing new one...");
                lessons = [];
            } else {
                throw new Error("Unable to read from storage. Wrong data format. " +
                    this._getStorageLocation());
            }
        }
        return lessons;
    }


    _isExists(lessons, id) {
        const result = lessons.find(b => {
            return b.id == id;
        });
        return result ? true : false;
    }


    _getStorageLocation() {
        return this.lessonStoragePath;
    }
}

module.exports = LessonsDao;