"use strict";
const fs = require("fs");
const path = require("path");
const bcrypt = require("bcrypt");

const rf = fs.promises.readFile;
const wf = fs.promises.writeFile;

// 1
const DEFAULT_STORAGE_PATH = path.join(__dirname, "storage", "users.json");

class UsersDao {
    constructor(storagePath) {
        this.userStoragePath = storagePath ? storagePath : DEFAULT_STORAGE_PATH;
    }

    // 2
    async getUser(id) {
        let users = await this._loadAllUsers();

        const result = users.find(b => {
            return b.id === id;
        });

        return result;
    }

    // 3
    async addUser(user) {
        const users = await this._loadAllUsers();

        if (this._isExists(users, user.email)) {
            const e = new Error(`User with email '${user.email}' already exists.`);
            e.code = "DUPLICATE_USER";
            throw e;
        }

        const lastId = users[users.length - 1].id;
        user.id = parseInt(lastId) + 1;

        users.push(user);

        try {
            await wf(this._getStorageLocation(), JSON.stringify(users, null, 2));
            return {status: "OK", data: user};
        } catch (e) {
            return {status: "ERROR", error: e};
        }

        return user;
    }

    async loginUser(user) {
        let users = await this._loadAllUsers();

        const result = users.find(b => {
            if (b.email !== user.email) {
                return false;
            }

            return bcrypt.compareSync(user.password, b.password_hash);
        });

        if (!result) {
            const e = new Error(`Bad login.`);
            e.code = "USER_BAD_LOGIN";
            throw e;
        }

        return result;
    }

    // 4
    async _loadAllUsers() {
        let users;
        try {
            users = JSON.parse(await rf(this._getStorageLocation()));
        } catch (e) {
            if (e.code === 'ENOENT') {
                console.info("No storage found, initializing new one...");
                users = [];
            } else {
                throw new Error("Unable to read from storage. Wrong data format. " +
                    this._getStorageLocation());
            }
        }
        return users;
    }


    _isExists(users, email) {
        const result = users.find(b => {
            return b.email === email;
        });
        return result ? true : false;
    }


    _getStorageLocation() {
        return this.userStoragePath;
    }
}

module.exports = UsersDao;