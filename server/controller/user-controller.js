const express = require("express");
const router = express.Router();

const CreateAbl = require("../abl/user/create-abl");
const GetAbl = require("../abl/user/get-abl");
const LoginAbl = require("../abl/user/login-abl");
const LogoutAbl = require("../abl/user/logout-abl");


router.get("/get", async (req, res) => {
    const {query} = req;
    await GetAbl(query, res)
});

router.post("/register", async (req, res) => {
    const {body, session} = req;
    await CreateAbl(body, res, session);
});

router.post("/login", async (req, res) => {
    const {body, session} = req;
    await LoginAbl(body, res, session);
});

router.post("/logout", async (req, res) => {
    await LogoutAbl(res, req.session);
});

router.get("/current", async (req, res) => {
    res.json(req.session.user);
});

module.exports = router