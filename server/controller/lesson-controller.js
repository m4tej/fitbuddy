const express = require("express");
const router = express.Router();

const CreateAbl = require("../abl/lesson/create-abl");
const GetAbl = require("../abl/lesson/get-abl");
const GetAllAbl = require("../abl/lesson/get-all-abl");
const JoinAbl = require("../abl/lesson/join-abl");
const LeaveAbl = require("../abl/lesson/leave-abl");
const DeleteAbl = require("../abl/lesson/delete-abl");
const UpdateAbl = require("../abl/lesson/update-abl");

router.get("/get", async (req, res) => {
    const {query} = req;
    await GetAbl(query, res)
});

router.get("/get-all", async (req, res) => {
    const {query} = req;
    await GetAllAbl(query, res)
});

router.post("/create", async (req, res) => {
    const {body} = req;
    await CreateAbl(body, res, req.session)
});

router.post("/update", async (req, res) => {
    const {body} = req;
    await UpdateAbl(body, res, req.session)
});

router.post("/delete", async (req, res) => {
    const {body} = req;
    await DeleteAbl(body, res);
});

router.post("/join", async (req, res) => {
    const {body} = req;
    await JoinAbl(body, res, req.session);
});

router.post("/leave", async (req, res) => {
    const {body} = req;
    await LeaveAbl(body, res, req.session);
});

module.exports = router;