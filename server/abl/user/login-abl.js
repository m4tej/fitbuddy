const path = require("path");
const UserDao = require("../../dao/user-dao");
const bcrypt = require("bcrypt");
let dao = new UserDao(path.join(__dirname, "..", "..", "storage", "users.json"))

async function LoginAbl(body, res, session) {
    if (!body.email || !body.password) {
        return res.status(400).json({error: 'Invalid input: parameter is missing.'});
    }

    try {
        const user = await dao.loginUser(body);
        session.user = user;
        session.save();
        res.json(user);

    } catch (e) {
        if (e.code === "USER_BAD_LOGIN") {
            res.status(404);
        } else {
            res.status(500);
        }
        return res.json({error: e.message});
    }


}

module.exports = LoginAbl;