const path = require("path");
const UserDao = require("../../dao/user-dao");
const bcrypt = require('bcrypt');
let dao = new UserDao(path.join(__dirname, "..", "..", "storage", "users.json"))

async function CreateAbl(body, res, session) {
    if (!body.email || !body.nickname || !body.password) {
        return res.status(400).json({error: 'Invalid input: parameter is missing.'});
    }

    const password_hash = await bcrypt.hash(body.password, 12);

    try {
        const user = await dao.addUser({
            email: body.email,
            nickname: body.nickname,
            password_hash,
        });

        session.user = user.data;
        session.save();
        res.json(user);

    } catch (e) {
        if (e.code === "DUPLICATE_USER") {
            res.status(400);
        } else {
            res.status(500);
        }
        return res.json({error: e.message});
    }

}

module.exports = CreateAbl;