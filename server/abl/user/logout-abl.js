const path = require("path");
const UserDao = require("../../dao/user-dao");
const bcrypt = require("bcrypt");
let dao = new UserDao(path.join(__dirname, "..", "..", "storage", "users.json"))

async function LogoutAbl(res, session) {
    delete session.user;
    session.save();
    res.status(204).send();
}

module.exports = LogoutAbl;