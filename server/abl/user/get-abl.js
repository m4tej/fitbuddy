const path = require("path");
const UserDao = require("../../dao/user-dao");
let dao = new UserDao(path.join(__dirname, "..", "..", "storage", "users.json"))

async function GetAbl(query, res) {
    const userId = query.id;
    if (!userId) {
        return res.status(400).json({error: 'Invalid input: code parameter is missing.'});
    }

    const user = await dao.getUser(userId);

    if (!user) {
        return res.status(400).json({error: `User with id '${userId}' doesn't exist.`});
    }

    res.json(user);
}

module.exports = GetAbl;