const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function LeaveAbl(body, res, session) {
    if (!session.user) {
        return res.status(400).json({error: 'User must be signed in'});
    }
    if (!body.lesson_id) {
        return res.status(400).json({error: 'Invalid input: parameter is missing.'});
    }

    try {
        await dao.leaveLesson(body.lesson_id, session.user.id);
        res.status(204).send();
    } catch (e) {
        if (e.code === "NOT_JOIN" || e.code === "NOT_FOUND") {
            res.status(400);
        } else {
            res.status(500);
        }
        return res.json({error: e.message});
    }

}

module.exports = LeaveAbl;