const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function CreateAbl(body, res, session) {
    if (!session.user) {
        return res.status(400).json({error: 'User must be signed in'});
    }
    if (!body.name || !body.date || !body.description || !body.capacity) {
        return res.status(400).json({error: 'Invalid input: parameter is missing.'});
    }

    const lesson = {
        name: body.name,
        date: body.date,
        description: body.description,
        capacity: body.capacity,
        owner_id: session.user.id,
        users_id: []
    };

    try {
        await dao.addLesson(lesson);
    } catch (e) {
        res.status(500);
        return res.json({error: e.message});
    }

    res.json(lesson);

}

module.exports = CreateAbl;