const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function GetAbl(query, res) {
    const lessonId = query.id;
    if (!lessonId) {
        return res.status(400).json({error: 'Invalid input: code parameter is missing.'});
    }

    const lesson = await dao.getLesson(lessonId);

    if (!lesson) {
        return res.status(400).json({error: `Lesson with id '${lessonId}' doesn't exist.`});
    }

    res.json(lesson);
}

module.exports = GetAbl;