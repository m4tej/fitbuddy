const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function DeleteAbl(body, res) {
    const lessonId = body.lesson_id;
    if (!lessonId) {
        return res.status(400).json({error: 'Invalid input: lesson_id parameter is missing.'});
    }

    try {
        await dao.deleteLesson(lessonId);
        res.status(204).send();
    } catch (e) {
        if (e.code === "NOT_FOUND") {
            res.status(400);
        } else {
            res.status(500);
        }
        return res.json({error: e.message});
    }

}

module.exports = DeleteAbl;