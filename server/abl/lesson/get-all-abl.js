const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function GetAllAbl(query, res) {
    const lessons = await dao._loadAllLessons();

    res.json(lessons);
}

module.exports = GetAllAbl;