const path = require("path");
const LessonDao = require("../../dao/lesson-dao");
let dao = new LessonDao(path.join(__dirname, "..", "..", "storage", "lessons.json"))

async function UpdateAbl(body, res, session) {
    if (!session.user) {
        return res.status(400).json({error: 'User must be signed in'});
    }
    if (body.id == null || !body.name || !body.date || !body.description || !body.capacity) {
        return res.status(400).json({error: 'Invalid input: parameter is missing.'});
    }

    const lesson = {
        id: body.id,
        name: body.name,
        date: body.date,
        description: body.description,
        capacity: body.capacity,
        owner_id: session.user.id,
    };

    try {
        await dao.updateLesson(lesson);
    } catch (e) {
        if (e.code === "NOT_FOUND") {
            res.status(400);
        } else if (e.code === "UNAUTHORIZED") {
            res.status(403);
        } else {
            res.status(500);
        }
        return res.json({error: e.message});
    }

    res.json(lesson);

}

module.exports = UpdateAbl;